﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;

namespace EFPostgreSQL6
{
    [Table("Testes", Schema = "public")]
    public class Teste
    {
        public Teste()
        {
        }

        [Key]
        public int TesteId { get; set; }

        public string TesteNome { get; set; }
    }
}