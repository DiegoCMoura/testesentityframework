﻿using System.Data.Entity;

namespace EFPostgreSQL6
{
    public class SampleDbContext : DbContext
    {
        public SampleDbContext()
            : base("name=SampleDbContext")
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Teste> Testes { get; set; }
    }
}