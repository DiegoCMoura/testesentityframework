﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using EFPostgreSQLCore;
using NpgsqlTypes;

namespace EFPostgreSQLCore.Migrations
{
    [DbContext(typeof(TesteContext))]
    [Migration("20171018022841_UpdateTableTestClassTable7")]
    partial class UpdateTableTestClassTable7
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDefaultSchema("public")
                .HasAnnotation("Npgsql:PostgresExtension:uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.3");

            modelBuilder.Entity("EFPostgreSQLCore.TesteClass", b =>
                {
                    b.Property<int>("TesteClassId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TesteClassName");

                    b.Property<PostgisGeometry>("TesteClassPoint")
                        .HasColumnType("geom(Point, 4326)");

                    b.Property<PostgisPoint>("TesteClassPoint2");

                    b.HasKey("TesteClassId");

                    b.ToTable("TesteClassTable");
                });
        }
    }
}
