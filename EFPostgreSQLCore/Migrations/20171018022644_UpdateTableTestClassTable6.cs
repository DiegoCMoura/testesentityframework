﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;

namespace EFPostgreSQLCore.Migrations
{
    public partial class UpdateTableTestClassTable6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<PostgisGeometry>(
                name: "TesteClassPoint2",
                schema: "public",
                table: "TesteClassTable",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TesteClassPoint2",
                schema: "public",
                table: "TesteClassTable");
        }
    }
}
