﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EFPostgreSQLCore.Migrations
{
    public partial class CreateTableTesteClassTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "TesteClassTable",
                schema: "public",
                columns: table => new
                {
                    TesteClassId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    TesteClassName = table.Column<string>(nullable: true),
                    TesteClassPoint = table.Column<NpgsqlPoint>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TesteClassTable", x => x.TesteClassId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TesteClassTable",
                schema: "public");
        }
    }
}
