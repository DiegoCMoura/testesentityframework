﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EFPostgreSQLCore.Migrations
{
    public partial class UpdateTableTestClassTable3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:.postgis", "'postgis', '', ''")
                .Annotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .OldAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .OldAnnotation("Npgsql:PostgresExtension:.postgis", "'postgis', '', ''")
                .OldAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''");
        }
    }
}
