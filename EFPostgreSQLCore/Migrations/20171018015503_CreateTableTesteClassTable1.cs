﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;

namespace EFPostgreSQLCore.Migrations
{
    public partial class CreateTableTesteClassTable1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", "'uuid-ossp', '', ''");

            migrationBuilder.AlterColumn<PostgisGeometry>(
                name: "TesteClassPoint",
                schema: "public",
                table: "TesteClassTable",
                nullable: true,
                oldClrType: typeof(NpgsqlPoint),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .OldAnnotation("Npgsql:PostgresExtension:uuid-ossp", "'uuid-ossp', '', ''");

            migrationBuilder.AlterColumn<NpgsqlPoint>(
                name: "TesteClassPoint",
                schema: "public",
                table: "TesteClassTable",
                nullable: true,
                oldClrType: typeof(PostgisGeometry),
                oldNullable: true);
        }
    }
}
