﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;

namespace EFPostgreSQLCore.Migrations
{
    public partial class UpdateTableTestClassTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<NpgsqlPoint>(
                name: "TesteClassPoint",
                schema: "public",
                table: "TesteClassTable",
                type: "geom(Point, 4326)",
                nullable: true,
                oldClrType: typeof(PostgisGeometry),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<PostgisGeometry>(
                name: "TesteClassPoint",
                schema: "public",
                table: "TesteClassTable",
                nullable: true,
                oldClrType: typeof(NpgsqlPoint),
                oldType: "geom(Point, 4326)",
                oldNullable: true);
        }
    }
}
