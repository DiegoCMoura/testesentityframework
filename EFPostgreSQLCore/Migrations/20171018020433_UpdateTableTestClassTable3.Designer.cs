﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using EFPostgreSQLCore;
using NpgsqlTypes;

namespace EFPostgreSQLCore.Migrations
{
    [DbContext(typeof(TesteContext))]
    [Migration("20171018020433_UpdateTableTestClassTable3")]
    partial class UpdateTableTestClassTable3
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasDefaultSchema("public")
                .HasAnnotation("Npgsql:PostgresExtension:uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.3");

            modelBuilder.Entity("EFPostgreSQLCore.TesteClass", b =>
                {
                    b.Property<int>("TesteClassId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TesteClassName");

                    b.Property<PostgisGeometry>("TesteClassPoint")
                        .HasColumnType("geom(Point, 4326)");

                    b.HasKey("TesteClassId");

                    b.ToTable("TesteClassTable");
                });
        }
    }
}
