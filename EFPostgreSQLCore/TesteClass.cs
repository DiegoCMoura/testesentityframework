﻿using System.Data.Entity.Spatial;
using NpgsqlTypes;

namespace EFPostgreSQLCore
{
    public class TesteClass
    {
        public int TesteClassId { get; set; }
        public string TesteClassName { get; set; }
        public PostgisGeometry TesteClassPoint { get; set; }
        public PostgisPoint TesteClassPoint2 { get; set; }
    }
}