﻿using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL;

namespace EFPostgreSQLCore
{
    public class TesteContext : DbContext
    {
        public TesteContext(DbContextOptions<TesteContext> options) : base(options)
        {
                
        }

        public virtual DbSet<TesteClass> TesteClassTable { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TesteClass>().Property(t => t.TesteClassPoint)
                .HasColumnType("geom(Point, 4326)");

            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Ignore<PluralizingTableNameConvention>();
            modelBuilder.HasPostgresExtension("uuid-ossp");

            base.OnModelCreating(modelBuilder);
        }
    }
}